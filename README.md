Sutty Restic Role
=================

Installs [Restic](https://restic.net/) locally and creates backups.

Requirements
------------

Most options are passed to restic via environment variables.  See
[dotenv role](https://0xacab.org/sutty/ansible/sutty_dotenv_role) to
create a temporary `.env` file.

We don't use the `environment` task option because it's exposed on the
process list.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_restic_static"
    vars:
      restic_version: "0.15.0"
      restic_platform: "linux_amd64"
      restic_directories:
      - "{{ ansible_env.HOME }}"
```

License
-------

MIT-Antifa
